# Hamster Web Server

[Live Server](http://transferencehamster.ddns.net:8080/)

```
curl -L https://gitlab.com/transference/hamster/hamster-web-server/-/raw/master/gce/startup-script.sh | sudo bash
```

## Changes from original

This code is derived from a repository on GitHub: <https://github.com/GoogleCloudPlatform/getting-started-python.git>. The initial commit contains raw non-derived code.

`/folder` means the folder named `folder` at the root of this repository.

* File `/README.md` is edited
* Folder `/gce` has additional files and folders (folders may have new files and/or folders):
    * Files
        * All files excluding: `/gce/README.md`, `/gce/deploy.sh`, `/gce/main.py`, `/gce/procfile`, `/gce/python-app.conf`, `/gce/requirements.txt`, `/gce/startup-script.sh`, `/gce/teardown.sh`
        * Note: the original files (stated one line above that says "All files...") may have been edited.
    * Folder
        * `/gce/preds`
        * `/gce/templates`

## Licensing

* See [LICENSE](LICENSE)
