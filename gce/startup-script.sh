# Copyright 2019 Google LLC All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# THIS FILE HAS BEEN EDITED FROM THE ORIGINAL.

# Echo commands
set -v

# [START getting_started_gce_startup_script]
echo '=============================='
echo '--/-- Hamster Web Server Startup Script v3'
echo '=============================='
echo '=============================='
echo '01/10 Install Stackdriver logging agent'
echo '=============================='
curl -sS0 https://dl.google.com/cloudagents/install-logging-agent.sh | sudo bash


echo '=============================='
echo '02/10 Update/Upgrade/Autoremove/Install Software'
echo '=============================='
apt-get update
apt-get upgrade -y
apt-get autoremove -y
apt-get install -y git supervisor python python-pip python3 python3-pip gunicorn gunicorn3
python3 -m pip install --upgrade pip virtualenv
python -m pip install --upgrade pip virtualenv

echo '=============================='
echo '03/10 Setup OS Environment'
echo '=============================='
sudo rm -r /opt/app

echo '=============================='
echo '04/10 Get Source Code'
echo '=============================='
export HOME=/root
git clone https://gitlab.com/transference/hamster/hamster-web-server.git /opt/app

echo '=============================='
echo '05/10 Setup Python Environment'
echo '=============================='
virtualenv -p python3 /opt/app/gce/env
source /opt/app/gce/env/bin/activate
# see https://stackoverflow.com/questions/29466663
/opt/app/gce/env/bin/pip --no-cache-dir install -r /opt/app/gce/requirements.txt
echo 'Patch py-nextbus #9'
echo 'Warn: This patch only works if using this script correctly!'
rm /opt/app/gce/env/lib/python3.5/site-packages/py_nextbus/client.py
curl -L https://raw.githubusercontent.com/colourdelete/py_nextbus/master/py_nextbus/client.py --output /opt/app/gce/env/lib/python3.5/site-packages/py_nextbus/client.py

echo '=============================='
echo '06/10 Setup Runner User'
echo '=============================='
useradd -m -d /home/pythonapp pythonapp
chown -R pythonapp:pythonapp /opt/app

echo '=============================='
echo '07/10 Run Server with nohup'
echo '=============================='
echo 'Run `ps ax | grep main_new.py` and `ps ax | grep live_daemon.py` to see process'
# (cd /opt/app/gce && python3 main_new.py)
(cd /opt/app/gce && nohup python main_new.py > main.log &)
ps ax | grep main_new.py
(cd /opt/app/gce && nohup python live_daemon.py > live_daemon.log &)
ps ax | grep live_daemon.py
# https://askubuntu.com/questions/396654


# echo '=============================='
# echo '07/10 Config Supervisor'
# echo '=============================='
# rm /etc/supervisor/conf.d/python-app.conf
# cp /opt/app/gce/python-app.conf /etc/supervisor/conf.d/python-app.conf

# echo '=============================='
# echo '08/10 Reread/Update Supervisor'
# echo '=============================='
# supervisorctl reread
# supervisorctl update
# supervisorctl restart all
# supervisord -c /etc/supervisor/conf.d/python-app.conf


echo '=============================='
echo '09/10 Config DDNS Software'
echo '=============================='
sudo /usr/local/bin/noip2
sudo /usr/local/bin/noip2 -S


echo '=============================='
echo '10/10 Done!'
echo '=============================='
# [END getting_started_gce_startup_script]
