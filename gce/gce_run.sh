echo "GCE Run Shell Script"
echo "Only use it for GCE stuff with custom sh script!!"


cd /opt/app/hamster-web
# export FLASK_APP='/opt/app/hamster-web/main.py:app'
# python3 -m flask run

source venv/bin/activate

python3 -m pip install grpcio # see https://github.com/grpc/grpc/issues/12992
python3 -m pip install -r requirements.txt
sudo gunicorn3 -w 4 -b 0.0.0.0:80 "main:app"

echo "Called/Done."
