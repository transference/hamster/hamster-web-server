from flask import *
import requests
import json
import csv
from geopy import distance
from datetime import datetime, date


def _nb_loc(id, a='ttc', r='51', t=0, all=False, dir=None):
  try:
    request = requests.get(
      'https://webservices.nextbus.com/service/publicJSONFeed?command=vehicleLocations&a={}&r={}&t={}'.format(a, r, t))
    data = json.loads(request.text)
  except Exception as err:
    print('JSON Decode may have failed; trying HTTP, not HTTPS')
    print('HTTPS request may have failed; trying HTTP, not HTTPS')
    request = requests.get(
      'http://webservices.nextbus.com/service/publicJSONFeed?command=vehicleLocations&a={}&r={}&t={}'.format(a, r, t))
    try:
      data = json.loads(request.text)
    except json.JSONDecodeError as err:
      return render_template('error.html', code='500-2', short='Location Fetch Failed',
                             long='Error encountered while fetching bus location data from an external server using '
                                  'HTTP, after HTTPS failed (probably SSL failed). JSON Decode Error.')
  del request
  if all:
    return data
  if dir != None:
    for vehicle in data['vehicle']:
      try:
        if vehicle['dirTag'].split('_')[2].lower() == dir.lower():
          return vehicle
      except KeyError:
        continue
  for vehicle in data['vehicle']:
    if vehicle['id'] == id:
      return vehicle
  for vehicle in data['vehicle']:
    if vehicle['predictable'] == 'true':
      return vehicle
    return data['vehicle'][0]


def _nb_pred(id, a='ttc', r='51', t=0, all=False, dir=None):
  request = requests.get(
    'https://webservices.nextbus.com/service/publicJSONFeed?command=vehicleLocations&a={}&r={}&t={}'.format(a, r, t))
  try:
    data = json.loads(request.text)
  except json.JSONDecodeError as err:
    print('JSON Decode Failed; trying HTTP, not HTTPS')
    request = requests.get(
      'http://webservices.nextbus.com/service/publicJSONFeed?command=vehicleLocations&a={}&r={}&t={}'.format(a, r, t))
    try:
      data = json.loads(request.text)
    except json.JSONDecodeError as err:
      return render_template('error.html', code='500-2', short='Location Fetch Failed',
                             long='Error encountered while fetching bus location data from an external server using '
                                  'HTTP, after HTTPS failed (probably SSL failed). JSON Decode Error.')
  del request
  if all:
    return data
  if dir != None:
    for vehicle in data['vehicle']:
      try:
        if vehicle['dirTag'].split('_')[2].lower() == dir.lower():
          return vehicle
      except KeyError:
        continue
  for vehicle in data['vehicle']:
    if vehicle['id'] == id:
      return vehicle
  for vehicle in data['vehicle']:
    if vehicle['predictable'] == 'true':
      return vehicle
    return data['vehicle'][0]


# def _gm_dist(coords_0, coords_1):
#   '''Enter in lon, lat format!'''
#   coords_0 = '{lat}, {lon}'.format(lon=coords_0[1], lat=coords_0[0])
#   coords_1 = '{lat}, {lon}'.format(lon=coords_1[1], lat=coords_1[0])
#   # GC Maps Directions API
#   # coords_0 = '43.70721,-79.3955999'
#   # coords_1 = '43.7077599,-79.39294'
#   with open('googlemaps_api_key.txt', 'r') as file:
#     api_key = file.read()
#   gmaps = googlemaps.Client(key=api_key)
#   # Request directions
#   now = datetime.now()
#   directions_result = gmaps.directions(coords_0, coords_1, mode="driving", departure_time=now, avoid='tolls')
  
#   distance = 0
#   print('dirresult', directions_result)
#   legs = directions_result[0].get("legs")
#   for leg in legs:
#     distance = distance + leg.get("distance").get("value")
#   return distance


def _gp_dist(coords_0, coords_1):
  return distance.distance(coords_0, coords_1).km


def _predict(sid, time, dist):
  print('Predicting: {} {} {}'.format(sid, time, dist))
  pred = 'Error Occurred (Unknown Error)'
  raw_time = time
  raw_lon = 0
  raw_lat = 0
  raw_dist = 0
  debug = 'No Debug Data'
  vehicle = 'No Vehicle'
  if time == 'now':
    today = date.today()
    raw_time = int(round(datetime.now().timestamp() * 1000))
    del today
    time = raw_time
  if sid == 'realdata':
    dist = int(dist)
    time = int(time)
    dist = -5 * 10 ** (-15) * time ** (2) + 5 * 10 ** (-7) * time
    time_0 = 10000000 * ((25 - 2 * dist) ** 0.5 + 5)
    time_1 = -10000000 * ((25 - 2 * dist) ** 0.5 + 5)
    if time_0 >= time_1:
      time_2 = time_0
    else:
      time_2 = time_1
    time = time_2 - time
  else:
    sids = sid.split('_')
    print('Details:    {} {} ({})'.format(sids[0], sids[1], sids[2]))
    if sid == 'ttc_34a_8887':
      vehicle = _nb_loc(sids[2], a=sids[0], r='34', all=False, dir=sids[1])
      # vehicle = _nb_loc('8644', a=sids[0], r='34', all=False, dir=sids[1]) # TODO: FIX HARDCODED SERIAL ID
      debug = _nb_loc(sids[2], a=sids[0], r='34', all=True)
      root_coords = [-79.400508, 43.705483]  # [lon, lat]
      dist = int(dist)
      time = int(time)
      raw_lon = vehicle['lon']
      raw_lat = vehicle['lat']
      bus_coords = [raw_lon, raw_lat]
      bus_dist = _gp_dist(root_coords, bus_coords)
      coef_0 = 6.30512972e-04
      coef_1 = -1.41784225e-02
      coef_2 = 1.35124852e-01
      coef_3 = 7.78586701e+00
      t = 6.30512972e-04 + -1.41784225e-02 * dist + 1.35124852e-01 * dist ** 2 + 7.78586701e+00 * dist ** 3
      t = coef_3 + coef_2 * dist + coef_1 * dist ** 2 + coef_0 * dist ** 3
      d_t = coef_3 + coef_2 * bus_dist + coef_1 * bus_dist ** 2 + coef_0 * bus_dist ** 3
      time_midnight = (datetime.now().timestamp() -
                       datetime.combine(date.today(), datetime.min.time()).timestamp())  # time_from_midnight
      time_midnight = round(time_midnight * 1000)
      time = time_midnight - t * 60 * 60 * 1000
      d_time = time_midnight - d_t * 60 * 60 * 1000
      if d_time - time >= time - d_time:
        time = d_time - time
      else:
        time = time - d_time
      raw_dist = bus_dist
      dist = bus_dist
    elif sid in preds.keys():
      with open(preds[sid], 'r') as file:
        data = list(csv.reader(file))
        for i in range(0, len(data) / 1000, 1000):
          if data[0] in range(time - 60000, time + 60000, 1):
            pred = data[1]
    else:
      return 1
  return {'sid': sid, 'time': time, 'dist': dist, 'raw_time': raw_time, 'raw_lon': raw_lon, 'raw_lat': raw_lat,
          'raw_dist': raw_dist, 'debug': debug, 'vehicle': vehicle,
          0: sid, 1: time, 2: dist, 3: raw_time, 4: raw_lon, 5: raw_lat, 6: raw_dist}
