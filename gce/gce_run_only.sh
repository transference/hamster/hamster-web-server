echo "GCE RUN ONLY Shell Script"
echo "RUN ONLY; DOES NOT INSTALL/UPDATE/UPGRADE"
echo "Only use it for GCE stuff with custom sh script!!"

# echo "Update software"
# sudo apt update
# sudo apt upgrade -y
# sudo apt autoremove -y
# sudo apt install -y git supervisor python3 python3-pip gunicorn3

cd /opt/app/hamster-web
# export FLASK_APP='/opt/app/hamster-web/main.py:app'
# python3 -m flask run

source venv/bin/activate

# python3 -m pip install -r requirements.txt
sudo gunicorn3 -w 4 -b 0.0.0.0:80 "main:app"

echo "Called/Done."
